var Accordion = function(el, multiple) {
  this.el = el || {};
  this.multiple = multiple || false;

  // Variables privadas
  var links = this.el.find('.b-accordion__title');
  // Evento
  links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
}

Accordion.prototype.dropdown = function(e) {
  var $el = e.data.el;
  var $this = $(this),
    $next = $this.next();

  $next.slideToggle();
  $this.parent().toggleClass('open');

  if (!e.data.multiple) {
    $el.find('.b-accordion__content').not($next).slideUp().parent().removeClass('open');
  }
}

$(document).ready(function() {
  var accordion = new Accordion($('.b-accordion'), false);
});

