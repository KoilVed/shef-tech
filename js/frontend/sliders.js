import Swiper from 'swiper';
import slick from 'slick-carousel';

document.addEventListener('DOMContentLoaded', function() {
  let gameSwiper = new Swiper('.js-purpose-slider', {
    slidesPerView: 3,
    spaceBetween: 35,
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction'
    },
    navigation: {
      nextEl: '.js-game-slider-next',
      prevEl: '.js-game-slider-prev'
    },
    breakpoints: {
      // when window width is >= 320px
      540: {
        slidesPerView: 1,
        spaceBetween: 0
      },
      // when window width is >= 480px
      768: {
        slidesPerView: 2,
        spaceBetween: 35
      },
      // when window width is >= 640px
      1024: {
        slidesPerView: 3,
        spaceBetween: 35
      }
    }
  });

  let aboutLastSlider = $('.js-about-last');
  let paramAboutLastSlider = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    dots: false
  };


  if ($(window).innerWidth() <= 600) {
    $('.b-about-history__progress--1 .b-about-history__path').remove();
    $(aboutLastSlider).slick(paramAboutLastSlider);
  }

  var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 0,
    slidesPerView: 4,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      500: {
        slidesPerView: 1,
        spaceBetween: 0
      },

      640: {
        slidesPerView: 2,
        spaceBetween: 0
      },
      1000: {
        slidesPerView: 3,
      }
    }
  });
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 0,
    navigation: {
      nextEl: '.js-next-top',
      prevEl: '.js-prev-top',
    },
    thumbs: {
      swiper: galleryThumbs,
    },
    effect: 'fade',
  });

  galleryTop.on('slideChange', function () {
    $('.b-catalog__slider-img-option').attr('data-state', this.realIndex)
  });
});
