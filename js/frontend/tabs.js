$(document).ready(function () {
  let linkTab = $('.js-tabs-items').children();
  let elClosest = '.b-tab';

  $(document).on('click', '.js-tabs-items > *', function (e) {
    let $this = $(this);
    let tabActive = $this.closest(elClosest).find('.js-tabs-items');
    let contentTab = $this.closest(elClosest).find('.js-tabs-contents').children();

    tabActive.children().removeClass('active');
    $this.addClass('active');
    let clickedTab = tabActive.find('.active');

    contentTab.removeClass('active');
    contentTab.eq(clickedTab.index()).addClass('active');
  });
});
